//Program to make a stack and store the string in it
//Stack shall have the following functions:
//1. Push
//2. Pop
//3. Peek
//4. Count
//5. Clear
//6. Constructor

#include <iostream>
#include <string>

using namespace std;

class Stack
{
    private:
        int data[100];
        int top;
    public:
        Stack()
        {
            top = -1;
        }

        void push(int v)
        {
            ++top;
            data[top] = v;  
        }  

        void pop()
        {
            cout<<data[top];
            top--;
        } 

        int count()
        {
            return top+1;
        }

        void clear()
        {
            cout<<"Clearing...";
            for(int i=top ; i > -1; i--)
            {
                cout<<data[i]<<" ";
                data[i] = 0;
            }
        }
};

int main()
{
    Stack test;
    test.push(1);
    test.push(2);
    test.push(3);
    test.push(4);
    test.push(5);
    test.push(6);
    test.push(7);
    test.push(8);
    test.push(9);
    cout<<"Number of elements"<<test.count()<<endl;
    test.pop();
    test.pop();
    cout<<"Number of elements"<<test.count()<<endl;
    test.clear();
}
